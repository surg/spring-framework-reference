[[spring-mvc-test-server-defining-expectations]]
====== Defining Expectations
Expectations can be defined by appending one or more `.andExpect(..)` after call to
perform the request:

[source,java,indent=0]
[subs="verbatim,quotes"]
----
	mockMvc.perform(get("/accounts/1")).andExpect(status().isOk());
----

`MockMvcResultMatchers.*` defines a number of static members, some of which return types
with additional methods, for asserting the result of the performed request. The
assertions fall in two general categories.

The first category of assertions verify properties of the response, i.e the response
status, headers, and content. Those are the most important things to test for.

The second category of assertions go beyond the response, and allow inspecting Spring
MVC specific constructs such as which controller method processed the request, whether
an exception was raised and handled, what the content of the model is, what view was
selected, what flash attributes were added, and so on. It is also possible to verify
Servlet specific constructs such as request and session attributes. The following test
asserts that binding/validation failed:

[source,java,indent=0]
[subs="verbatim,quotes"]
----
	mockMvc.perform(post("/persons"))
		.andExpect(status().isOk())
		.andExpect(model().attributeHasErrors("person"));
----

Many times when writing tests, it's useful to dump the result of the performed request.
This can be done as follows, where `print()` is a static import from
`MockMvcResultHandlers`:

[source,java,indent=0]
[subs="verbatim,quotes"]
----
	mockMvc.perform(post("/persons"))
		.andDo(print())
		.andExpect(status().isOk())
		.andExpect(model().attributeHasErrors("person"));
----

As long as request processing causes an unhandled exception, the `print()` method will
print all the available result data to `System.out`.

In some cases, you may want to get direct access to the result and verify something that
cannot be verified otherwise. This can be done by appending `.andReturn()` at the end
after all expectations:

[source,java,indent=0]
[subs="verbatim,quotes"]
----
	MvcResult mvcResult = mockMvc.perform(post("/persons")).andExpect(status().isOk()).andReturn();
	// ...
----

When all tests repeat the same expectations, you can define the common expectations once
when building the `MockMvc`:

[source,java,indent=0]
[subs="verbatim,quotes"]
----
	standaloneSetup(new SimpleController())
		.alwaysExpect(status().isOk())
		.alwaysExpect(content().contentType("application/json;charset=UTF-8"))
		.build()
----

Note that the expectation is __always__ applied and cannot be overridden without
creating a separate `MockMvc` instance.

When JSON response content contains hypermedia links created with
https://github.com/spring-projects/spring-hateoas[Spring HATEOAS], the resulting links can
be verified:

[source,java,indent=0]
[subs="verbatim,quotes"]
----
	mockMvc.perform(get("/people").accept(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.links[?(@.rel == ''self'')].href").value("http://localhost:8080/people"));
----

When XML response content contains hypermedia links created with
https://github.com/spring-projects/spring-hateoas[Spring HATEOAS], the resulting links can
be verified:

[source,java,indent=0]
[subs="verbatim,quotes"]
----
	Map<String, String> ns = Collections.singletonMap("ns", "http://www.w3.org/2005/Atom");
	mockMvc.perform(get("/handle").accept(MediaType.APPLICATION_XML))
		.andExpect(xpath("/person/ns:link[@rel=''self'']/@href", ns).string("http://localhost:8080/people"));
----

[[spring-mvc-test-server-filters]]
====== Filter Registrations
When setting up a `MockMvc`, you can register one or more `Filter` instances:

[source,java,indent=0]
[subs="verbatim,quotes"]
----
	mockMvc = standaloneSetup(new PersonController()).addFilters(new CharacterEncodingFilter()).build();
----

Registered filters will be invoked through `MockFilterChain` from `spring-test` and the
last filter will delegates to the `DispatcherServlet`.

